package com.company;

import java.util.Objects;

public class PokePoint {
    private String name;
    private int score = 1;

    public PokePoint(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void giveScore() {
        this.score++ ;
    }

    public boolean isBigger(PokePoint p){
        if (this.score > p.score)
            return false;
        return true;
    }

    public String getName() {
        return name;
    }




}
