package com.company;

import java.util.ArrayList;

public class Player {
    private String name;
    private ArrayList<PokePoint> scoreList = new ArrayList();


    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPokemon(){
        PokePoint pp = new PokePoint("No Pokemon!");

        for(PokePoint p : scoreList){
            if (pp.isBigger(p)){
                pp = p;
            }
        }
        return pp.getName();
    }

    public void addPokePint(String pokeName){
        for (PokePoint p : scoreList){
            if (p.getName().equals(pokeName)){
                p.giveScore();
                return;
            }
        }
        scoreList.add(new PokePoint(pokeName));
    }


}
